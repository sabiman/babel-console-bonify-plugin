const babel = require('babel-core');
const plugin = require('../');

describe('Test babel console.log bonificator plugin', () => {

    it('should bonify console.log', () => {
        const given = `var i = 1;console.log('blabla');`;
        const result = `var i = 1;console.log('Bonify rocks' + 'blabla');`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should bonify first argument of console.log', () => {
        const given = `var i = 1;console.log({ a: 1 }, second, third);`;
        const result = `var i = 1;console.log("Bonify rocks" + { a: 1 }, second, third);`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should not bonify other methods of console as console.err', () => {
        const given = `var i = 1;console.err('error');`;
        const result = `var i = 1;console.err('error');`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should bonify of console.log.call', () => {
        const given = `var i = 1;console.log.call(null, a);`;
        const result = `var i = 1;console.log.call(null, "Bonify rocks" + a);`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should bonify of console.log.apply', () => {
        const given = `var a = [1, 2];console.log.apply(null, a);`;
        const result = `var a = [1, 2];console.log.apply(null, ["Bonify rocks", ...a]);`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should bonify several console.logs', () => {
        const given = `var i = 1;console.log('blabla');var j = i + 1;console.log(j);`;
        const result = `var i = 1;console.log('Bonify rocks' + 'blabla');var j = i + 1;console.log('Bonify rocks' + j);`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });

    it('should bonify only global console.log', () => {
        const given = `var i = 1;console.log('blabla');function f() {var console = {log: ()=>{}};console.log('a');};`;
        const result = `var i = 1;console.log('Bonify rocks' + 'blabla');function f() {
  var console = { log: () => {} };console.log('a');
};`;
        const {code} =  babel.transform(given, {plugins: [plugin]});

        expect(code).toBe(result);
    });
});

