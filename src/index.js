"use strict";

module.exports = function({ types: t }) {

    const bonificator = 'Bonify rocks';

    // transform to the sum with a string literal f.e.: a -> str + a
    const bonifyNodeAsBinaryExpr = path => {
        path.replaceWith(t.binaryExpression('+', t.StringLiteral(bonificator), path.node))
    };

    // convert arrayLike to Array with a string literal as a first element f.e  arr -> [str, ...arr]
    const bonifyNodeAsArray = path => {
        path.replaceWith(t.arrayExpression([t.StringLiteral('Bonify rocks'), t.spreadElement(path.node)]));
    };

    //check that object is in global scope and is named 'console'
    const isGlobalConsoleObject = obj => {
        const name = "console";
        return obj.isIdentifier({ name })
                && !obj.scope.getBinding(name)
                && obj.scope.hasGlobal(name);
    };

    return {
        name: 'transform-bonify-console',

        visitor: {
            CallExpression(path, state) {
                const callee = path.get('callee');
                // don't look at non member function calls...
                if (!callee.isMemberExpression()) return;

                const obj = callee.get('object');
                const property = callee.get("property");
                const args = path.get('arguments');

                // check for global console.log
                if( isGlobalConsoleObject(obj) && property.isIdentifier({name: 'log'}) ) {
                    bonifyNodeAsBinaryExpr(args[0]);
                }

                //check for global console.log.call or console.log.apply
                if( isGlobalConsoleObject( obj.get('object')) 
                    && obj.node.property.name === 'log' ) {

                    if( property.isIdentifier({ name: "call" })) {
                    // change second argument to binary expression for 'call'
                        bonifyNodeAsBinaryExpr( args[1] );
                    }

                    if( property.isIdentifier({ name: "apply" })) {
                    // bit hacky for 'apply':
                    // transforms console.log.apply(thisArg, arr) to console.log.apply(thisArg, [str, ...arr])
                    // the result is not the same as for other transformations, since it passes str as another argument
                    // to console.log, but not as str + arr[0]
                    // but since this works the same for console.log, and reduces the efforts for transformation itself - let it be...
                        bonifyNodeAsArray(args[1]);
                    }
                } 
            }
        }
    };
};