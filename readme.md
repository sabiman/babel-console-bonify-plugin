
## Bonification babel-plugin-bonify

Babel transformation plugin, that replaces all console.log(n) calls to console.log("Bonify rocks" + n);

## Install

Using npm:

```sh
npm install --save-dev @sabiman/babel-plugin-bonify
```

Following use cases were tested:

## case 1

#### one argument

```javascript
console.log(n)

      ↓ ↓ ↓ ↓ ↓ ↓

console.log("Bonify rocks" + n);
```

## case 2

#### several arguments

```javascript
console.log(n , m, k, ...)

      ↓ ↓ ↓ ↓ ↓ ↓

console.log("Bonify rocks" + n, m, k, ...);
```

## case 3

#### global scope only

```javascript
console.log(n);
let a = function(console) {
            let console = {
                log: otherLogFn
            }
            console.log(n);
        };

      ↓ ↓ ↓ ↓ ↓ ↓

console.log("Bonify rocks" + n);
let a = function(console) {
            let console = {
                log: otherLogFn
            };
            console.log(n);
        };
```

## case 4

#### .call

```javascript
console.log.call(obj, n , m, k, ...)

      ↓ ↓ ↓ ↓ ↓ ↓

console.log.call(obj, "Bonify rocks" + n, m, k, ...);
```

## case 4

#### .apply

```javascript
console.log.apply(obj, [n , m, k]);

      ↓ ↓ ↓ ↓ ↓ ↓

console.log.apply(obj, ["Bonify rocks", ...[n, m, k]]);
```
